import numpy as np
import json
import requests


#测试seldon  mlflow 服务
nn = np.random.rand(1,1,28,28)

inference_request={"parameters": {"content_type": "np"},"inputs": [{"datatype": "FP32", "shape": [1,1,28,28],"data": [1,2,3,4,5,6], "name": "INPUT__0"}]}
inference_request['inputs'][0]['data'] = nn.tolist()

endpoint = "http://10.42.4.61:9000/v2/models/mlflow/infer"
response = requests.post(endpoint, json=inference_request)
print(response.content)
